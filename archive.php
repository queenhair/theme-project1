<?php get_header();

$term = get_queried_object();
if($term->taxonomy == 'product_category'){
    get_template_part('taxonomy-shop');
}
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry' ); ?>
<?php endwhile; endif; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</main>
<?php get_footer(); ?>