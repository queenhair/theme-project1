<div class="content-area">
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <p style="font-size:34px; margin:0;"><?php echo the_title();?></p>
            </div>
            <ul class="breadcrumb">
                <?php if(function_exists('bcn_display'))
                {
                    bcn_display();
                }?>
            </ul>
        </div>
    </section>
    <section class="page-section with-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12 content entry" id="content">
                    <!-- Blog post -->
                    <div class="entry-content">
                        <?php
                        the_content(
                            sprintf(
                                wp_kses(
                                    /* translators: %s: Post title. Only visible to screen readers. */
                                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
                                    array(
                                        'span' => array(
                                            'class' => array(),
                                        ),
                                    )
                                ),
                                get_the_title()
                            )
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>