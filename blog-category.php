<?php $taxnomy = get_queried_object();  ?>
<?php

global $GMOPlugin;
$optionTheme  = $GMOPlugin->themeSetting->getSettings();
$pageId = get_the_ID();

$argsAll = array(
    'posts_per_page' => -1,
    'post_type'   => 'post',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $taxnomy->term_id,
        )
    )
);
$itemPerPage = !empty($optionTheme['gmo_general_product_item_per_post']) ? $optionTheme['gmo_general_product_item_per_post'] : 10;
$currentPage = !empty($_GET['gpage']) ? $_GET['gpage'] : 1;
$offset = ($currentPage - 1) * $itemPerPage;
$totalItem = count(get_posts($argsAll));
$totalPage = ceil($totalItem/$itemPerPage);


$argsBlog = [
    'posts_per_page' => -1,
    'post_type'   => 'post',
    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $taxnomy->term_id,
        )
    ),
    'offset' => $offset
];
$blogList = get_posts($argsBlog);
?>
<div class="content-area">

    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <h1><?php echo $taxnomy->name?></h1>
            </div>
            <ul class="breadcrumb">
                <li><a href="<?php echo home_url() ?>">Home</a></li>
                <li class="active"><?php echo $taxnomy->name ?></li>
            </ul>
        </div>
    </section>
    <!-- /BREADCRUMBS -->

    <!-- PAGE WITH SIDEBAR -->
    <section class="page-section with-sidebar">
        <div class="container">
            <div class="row">
                <!-- CONTENT -->
                <?php if(!empty($blogList[0])):?>
                    <?php foreach($blogList as $blog):?>
                        <div class="col-md-6 content" id="content">

                            <article class="post-wrap">
                            <div class="post-media">
                                <a href="assets/img/preview/blog/blog-post-1.jpg" data-gal="prettyPhoto"><img src="<?php echo get_the_post_thumbnail_url( $blog->ID, 'gmo-thumbnail-555x236' ); ?>" alt=""></a>
                            </div>
                            <div class="post-header">
                                <h2 class="post-title"><a href="<?php echo get_the_permalink($blog->ID)?>">  <?php echo $blog->post_title ?></a></h2>
                            </div>
                            <div class="post-body">
                                <div class="post-excerpt">
                                    <p><?php echo wp_trim_words($blog->post_content,20) ?>...</p>
                                </div>
                            </div>
                            <div class="post-footer">
                                <span class="post-read-more"><a href="<?php echo get_the_permalink($blog->ID)?>" class="btn btn-theme btn-theme-transparent btn-icon-left"><i class="fa fa-file-text-o"></i>Read more</a></span>
                            </div>
                        </article>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
                <div class="col-md-12 content" id="content">
                    <?php if($totalPage > 1): ?>
                        <div class="pagination-wrapper">
                            <ul class="pagination">
                                <?php if($currentPage > 1):?>
                                    <li class="disabled"><a href="?gpage=1"><i class="fa fa-angle-double-left"></i>Previous</a></li>
                                <?php endif ?>
                                <?php for($num = 1; $num <= $totalPage; $num++): ?>
                                    <?php if($num == $currentPage):?>
                                        <li class="active">
                                            <a style="background-color: rgba(35, 35, 35, 0.1);" href="?gpage=<?php echo $num ?>"><?php echo $num ?>
                                                <span class="sr-only">(current)</span>
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <li><a href="?gpage=<?php echo $num ?>"><?php echo $num ?></a></li>
                                    <?php endif ?>
                                <?php endfor ?>
                                <?php if($currentPage < $totalPage):?>
                                    <li><a href="?gpage=<?php echo $totalPage?>">Next <i class="fa fa-angle-double-right"></i></a></li>
                                <?php endif ?>
                            </ul>
                        </div>
                    <?php endif ?>
                    <!-- /Pagination -->

                </div>
                <!-- /CONTENT -->

            </div>
        </div>
    </section>
    <!-- /PAGE WITH SIDEBAR -->

</div>