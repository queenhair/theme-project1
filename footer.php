<!-- FOOTER -->
<?php
global $GMOPlugin;
$optionTheme  = $GMOPlugin->themeSetting->getSettings(); 
?>
<footer class="footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="widget">
                        <?php
                            if (is_active_sidebar('bas-footer-about')) {
                                dynamic_sidebar('bas-footer-about');
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="widget">
                            <?php
                            if (is_active_sidebar('bas-footer-about2')) {
                                dynamic_sidebar('bas-footer-about2');
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <?php
                            if (is_active_sidebar('bas-footer-info')) {
                                dynamic_sidebar('bas-footer-info');
                            }
                            ?>
                    </div>
                    <div class="col-md-3">
                    <?php
                            if (is_active_sidebar('bas-footer-tag')) {
                                dynamic_sidebar('bas-footer-tag');
                            }
                            ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer-meta">
            <div class="container">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="copyright"><?php echo !empty($optionTheme['gmo_footer_copyright']) ? $optionTheme['gmo_footer_copyright'] : ''?></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="payments">
                            <ul>
                                <li><img src="assets/img/preview/payments/visa.jpg" alt=""/></li>
                                <li><img src="assets/img/preview/payments/mastercard.jpg" alt=""/></li>
                                <li><img src="assets/img/preview/payments/paypal.jpg" alt=""/></li>
                                <li><img src="assets/img/preview/payments/american-express.jpg" alt=""/></li>
                                <li><img src="assets/img/preview/payments/visa-electron.jpg" alt=""/></li>
                                <li><img src="assets/img/preview/payments/maestro.jpg" alt=""/></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </footer>
    <!-- /FOOTER -->

    <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>

</div>
<!-- /WRAPPER -->
<div class="hotline-phone-ring-wrap">
    <div class="hotline-phone-ring">
        <div class="hotline-phone-ring-circle"></div>
        <div class="hotline-phone-ring-circle-fill"></div>
        <div class="hotline-phone-ring-img-circle">
            <a href="https://wa.me/84967894448" class="pps-btn-img">
                <img src="https://k-hair.com/wp-content/themes/khair/images/phone-icon-white.png" alt="+84967894448" width="50">
            </a>
        </div>
    </div>
    <div class="hotline-bar">
        <a href="https://wa.me/84967894448">
            <span class="text-hotline">+84967894448</span>
        </a>
    </div>
</div>
<!-- JS Global -->
<?php wp_footer() ?>
</body>
</html>